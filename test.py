import sqlite3

conn = sqlite3.connect("cars.db")

cursor = conn.cursor()

cursor.execute("""CREATE table inventory (Make TEXT, Model TEXT, Quantity INT)""")

conn.close()