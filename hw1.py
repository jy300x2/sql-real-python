import sqlite3

with sqlite3.connect('cars.db') as connection:
    c = connection.cursor()
    
    # cars = [
    #     ("Ford", "Focus", 1),
    #     ("Ford", "Fusion", 11),
    #     ("Ford", "Escape", 2),
    #     ("Honda", "Civic", 34),
    #     ("Honda", "Accord", 56),
    # ]
    
    # c.executemany("INSERT INTO inventory VALUES(?, ? ,?)", cars)
    
    # c.execute("UPDATE inventory SET quantity = 100 WHERE model = 'Accord' ")
    # c.execute("UPDATE inventory SET model = 'F-150' where model = 'Focus' ")
    
    c.execute("SELECT * FROM inventory WHERE make = 'Ford'")
    
    rows = c.fetchall()
    
    for i in rows:
        print(i[0], i[1], i[2])

