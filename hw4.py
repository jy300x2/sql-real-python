import sqlite3

with sqlite3.connect("cars.db") as connection:
    c = connection.cursor()
    
    sql = {
        'Focus': "SELECT count(make) FROM orders WHERE model = 'Focus'",
        'Fusion': "SELECT count(make) FROM orders WHERE model = 'Fusion'",
        'Escape': "SELECT count(make) FROM orders WHERE model = 'Escape'",
        'Civic': "SELECT count(make) FROM orders WHERE model = 'Civic'",
        'Accord': "SELECT count(make) FROM orders WHERE model = 'Accord'"
    }
    
    for keys, values in sql.items():
        
        c.execute(values)
    
        result = c.fetchone()
    
        print(keys + ":", result[0])