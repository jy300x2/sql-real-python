import sqlite3

with sqlite3.connect("cars.db") as connection:
    c = connection.cursor()
    
    c.execute("CREATE TABLE orders(make TEXT, model TEXT, order_date TEXT)")
    
    car_orders = [
         ("Ford", "Focus", '2017-09-20'),
         ("Ford", "Focus", '2017-09-06'),
         ("Ford", "Focus", '2017-09-15'),
         
         ("Ford", "Fusion", '2016-10-12'),
         ("Ford", "Fusion", '2016-10-11'),
         ("Ford", "Fusion", '2016-10-08'),
         
         ("Ford", "Escape", '2015-2-22'),
         ("Ford", "Escape", '2015-2-23'),
         ("Ford", "Escape", '2015-2-27'),
         
         ("Honda", "Civic", '2017-05-21'),
         ("Honda", "Civic", '2017-05-01'),
         ("Honda", "Civic", '2017-05-10'),
         
         ("Honda", "Accord", '2017-03-04'),
         ("Honda", "Accord", '2017-03-07'),
         ("Honda", "Accord", '2017-03-05'),
    ]
    
    c.executemany("INSERT INTO orders VALUES(?, ?, ?)", car_orders)
    
    c.execute("SELECT * FROM orders")
    
    rows = c.fetchall()
    
    for r in rows:
        print(r[0], r[1], r[2])
    